# NodeJS Entity Files Generator

This apllication will generate the files required for an entity, such as controller, validator, router, service, etc.

#### Usage

```sh
node index.js <slug-name>
```

#### Output

You will find the files in the _output_ folder.