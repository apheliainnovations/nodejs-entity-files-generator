// const fs = require('fs')
// const filePath = './data/initialized.js'

// fs.mkdir('/data', { recursive: true }, (err) => {
//     if (err) throw err;

//     else fs.closeSync(fs.openSync(filePath, 'w'));
// });

const fs = require('fs');
const path = require('path');

const directory = 'output';
const slug = process.argv[2];

if (!slug) {
    throw new Error('Please provide the slug using the command \'node index.js <slug-name>\'');
}

fs.readdir(directory, (err, files) => {
  if (err) throw err;

  for (const file of files) {
    fs.unlink(path.join(directory, file), err => {
      if (err) throw err;
    });
  }

  fs.closeSync(fs.openSync(`${directory}/${slug}.controller.js`, 'w'));
  fs.closeSync(fs.openSync(`${directory}/${slug}.validator.js`, 'w'));
  fs.closeSync(fs.openSync(`${directory}/${slug}.service.js`, 'w'));
  fs.closeSync(fs.openSync(`${directory}/${slug}.router.js`, 'w'));
  fs.closeSync(fs.openSync(`${directory}/${slug}.test.js`, 'w'));
});

